# toolchain

This is my take on the GNU toolchain for [Arch Linux](https://archlinux.org).
I'm especially interested in:

- [bootstrapping](https://bugs.archlinux.org/task/70954)
- [reproducibility](https://wiki.archlinux.org/title/Reproducible_builds)
- optimization i.e. streamlining the build procedure

Pretty much everything documented in [my pkgbuilds repo][pkgbuilds] also
applies here.

[pkgbuilds]: https://gitlab.com/Toolybird/pkgbuilds/-/blob/main/README.md

## Setup

Included is a build script and some directories containing the PKGBUILDs.
Each directory represents a different toolchain e.g.

- "arch" - a placeholder directory for current upstream Arch trunk. It can
be populated with: `asp export binutils gcc glibc linux-api-headers`
- "devel" - my modifications, proposed fixes, hacks, etc.

The build script expects a toolchain "dir" to be set up as follows:

\<dir\>/binutils/PKGBUILD  
\<dir\>/gcc/PKGBUILD  
\<dir\>/glibc/PKGBUILD  
\<dir\>/linux-api-headers/PKGBUILD  

To assist with binary comparisons and looking into reproducibility issues,
the build script supports building multiple toolchains. For example:

    ./build-toolchain /path/to/chroot arch devel

Preserving chroot "/build/..." dirs (the -k option) is quite useful when
investigating problems but please be aware this will consume **lots** of
disk space.

## Compilation Speed

Recent Arch developments have *dramatically* slowed down the build. GCC is
particularly painful. This has happened because of:

- [LTO](https://gitlab.archlinux.org/archlinux/rfcs/-/blob/master/rfcs/0004-lto-by-default.rst)
- [PGO](https://en.wikipedia.org/wiki/Profile-guided_optimization)
- [Debug packages](https://archlinux.org/news/debug-packages-and-debuginfod/)
- New language support e.g. libgccjit

combined with a bootstrap procedure which has become inefficient due to
the need for reproducibility. Therefore, building on a beefy machine with
plenty of CPU cores is recommended.
